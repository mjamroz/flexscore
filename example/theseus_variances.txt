    #ATOM   resName resSeq     variance      std_dev         RMSD
RES 1           HIS    529     2.340185     1.529766     3.811205 CORE
RES 2           MET    530     0.690328     0.830860     2.069974 CORE
RES 3           THR    531     0.584148     0.764296     1.904139 CORE
RES 4           PRO    532     0.386475     0.621671     1.548810 CORE
RES 5           GLN    533     0.343077     0.585727     1.459261 CORE
RES 6           ASP    534     0.263306     0.513134     1.278404 CORE
RES 7           HIS    535     0.113850     0.337417     0.840628 CORE
RES 8           GLU    536     0.056549     0.237801     0.592448 CORE
RES 9           LYS    537     0.058244     0.241337     0.601260 CORE
RES 10          ALA    538     0.046970     0.216726     0.539944 CORE
RES 11          ALA    539     0.085111     0.291738     0.726825 CORE
RES 12          LEU    540     0.099915     0.316094     0.787505 CORE
RES 13          ILE    541     0.048358     0.219905     0.547865 CORE
RES 14          MET    542     0.090550     0.300915     0.749688 CORE
RES 15          GLN    543     0.127457     0.357011     0.889444 CORE
RES 16          VAL    544     0.162506     0.403120     1.004319 CORE
RES 17          LEU    545     0.241507     0.491433     1.224340 CORE
RES 18          GLN    546     0.512920     0.716184     1.784275 CORE
RES 19          LEU    547     0.910839     0.954379     2.377705 CORE
RES 20          THR    548     1.357767     1.165233     2.903019 CORE
RES 21          ALA    549     1.031850     1.015800     2.530728 CORE
RES 22          ASP    550     1.377596     1.173711     2.924141 CORE
RES 23          GLN    551     1.030941     1.015353     2.529613 CORE
RES 24          ILE    552     0.444016     0.666346     1.660110 CORE
RES 25          ALA    553     0.517510     0.719381     1.792241 CORE
RES 26          MET    554     0.452476     0.672663     1.675849 CORE
RES 27          LEU    555     0.148745     0.385675     0.960858 CORE
RES 28          PRO    556     0.112405     0.335268     0.835274 CORE
RES 29          PRO    557     0.385827     0.621150     1.547511 CORE
RES 30          GLU    558     0.400361     0.632741     1.576388 CORE
RES 31          GLN    559     0.152474     0.390479     0.972825 CORE
RES 32          ARG    560     0.020428     0.142927     0.356083 CORE
RES 33          GLN    561     0.028579     0.169052     0.421170 CORE
RES 34          SER    562     0.023666     0.153837     0.383264 CORE
RES 35          ILE    563     0.026302     0.162180     0.404050 CORE
RES 36          LEU    564     0.043998     0.209757     0.522580 CORE
RES 37          ILE    565     0.027621     0.166195     0.414053 CORE
RES 38          LEU    566     0.027299     0.165224     0.411634 CORE
RES 39          LYS    567     0.119755     0.346057     0.862153 CORE
RES 40          GLU    568     0.115460     0.339794     0.846552 CORE
RES 41          GLN    569     0.052896     0.229991     0.572991 CORE
RES 42          ILE    570     0.084061     0.289932     0.722326 CORE
RES 43          GLN    571     0.139517     0.373520     0.930575 CORE
RES 44          LYS    572     0.221217     0.470337     1.171782 CORE
RES 45          SER    573     2.718619     1.648824     4.107820 CORE
RES 46          THR    574     8.473226     2.910881     7.252064 CORE
RES 47          GLY    575    18.128343     4.257739    10.607580 CORE
RES 48          ALA    576    26.265675     5.125005    12.768255 CORE
RES 49          PRO    577    34.029673     5.833496    14.533364 CORE

