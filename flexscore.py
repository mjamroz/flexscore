#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# File:           flexscore.py
#
# Function:       Calculate dissimilarity score between predicted model
#                 of protein structure and the native-state structural ensemble
#
# Author(s):      Michal Jamroz jamroz@chem.uw.edu.pl
# Copyright:      Copyright (c) 2016 Michal Jamroz
#
# You should have received a copy of the MIT
# License along with this file in the file 'COPYING'

import ctypes
import re
import os
import numpy as np
import argparse


def calcMah(filename, centroid, weights, output=False, score_res=False):
    '''
        Function returns average Z-score, i.e. how many std deviations
        is considered structure to the ensemble mean, on average.
    '''
    # structure is N,3 array of coordinates
    # resids is N list of residue indexes as in PDB file
    # seqids is N list of sequence names as in PDB file
    structure, resids, seqids = readCoordinates(filename)

    # c - centroid/mean (\hat{M}), w - betas (Sigma^{-1})
    t_c = np.average(centroid, axis=0, weights=weights)

    c, w = centroid, weights

    assert structure.shape == c.shape, ("%r structure length %r differs "
                                        "from ensemble centroid length: %r"
                                        % (filename, structure.shape, c.shape))
    # superimposition structure into native-state ensemble
    # Eq 10 10.1093/bioinformatics/bts243
    t = np.average(structure, axis=0, weights=w)
    # Eq 14 10.1093/bioinformatics/bts243
    structure = np.subtract(structure, t)
    # Eq 16 10.1093/bioinformatics/bts243
    U, _, V = np.linalg.svd(np.dot((c - t_c).T*w, structure))

    if np.linalg.det(U) * np.linalg.det(V) < 0:
        P = np.diag([1, 1, -1])
    else:
        P = np.diag([1, 1, 1])
    # Eq. 15 10.1093/bioinformatics/bts243
    vp = np.dot(np.dot(V.T, P), U.T)
    rotated = (structure).dot(vp) + t_c
    #debug/ print "RMSD", _rmsd(structure,centroid), rmsd_cur(rotated, centroid)


    u = np.subtract(rotated, c)
    dists = (u*u).sum(axis=1)
    # z_vals = Zscore2 = \frac{(\mu_i - x_i)^2}{\sigma_^2}
    z_vals = dists * w
    # FS = 1/N \sum_i^N sqrt(z_vals)
    mah_real = np.average(np.sqrt(z_vals))


    # save rotated coordinates to 'output' filename
    if output:
        _savePdb(output, seqids, resids, rotated)
    # save score/residue
    if score_res:
        _saveScore(filename, seqids, resids, np.sqrt(z_vals))

    return mah_real


def calcMahWithGDT(filename, centroid, weights):
    '''
        Calculate GDT-like FlexScore.
    '''

    structure, resids, seqids = readCoordinates(filename)
    return _gdt_tm_superimposeFS(structure, centroid, weights)

def _gdt_tm_superimposeFS(x, y, weights, L=None, d0=None, L_ini_min=4,
        iL_step=1, rres=None, sseq=None):
    """
    Compute the GDT_TS / modification of method from CSB library
    Reference:  http://zhanglab.ccmb.med.umich.edu/TM-score / CSB toolkit
    """

    centroid = y
    inv_weights = 1.0/np.sqrt(weights)

    x, y = np.asarray(x), np.asarray(y)
    if not L:
        L = len(x)
    if not d0:
        d0 = _tm_d0(L)
    d0_search = np.clip(d0, 4.5, 8.0)
    best = 0.0

    gdt1248 = [0.0, 0.0, 0.0, 0.0]
    fsgdt1248 = [0.0, 0.0, 0.0, 0.0]
    fsgdt1248_sigma = [0.0, 0.0, 0.0, 0.0]

    L_ini_min = min(L, L_ini_min) if L_ini_min else L
    L_ini = [L_ini_min] + list(filter(lambda x: x > L_ini_min,
            [L // (2 ** n_init) for n_init in range(6)]))

    # the outer two loops define a sliding window of different sizes for the
    # initial local alignment (disabled with L_ini_min=0)
    old = 0
    for L_init in L_ini:
        for iL in range(0, L - L_init + 1, min(L_init, iL_step)):
            mask = np.zeros(L, bool)
            mask[iL:iL + L_init] = True

            # refine mask until convergence, similar to fit_wellordered
            for i in range(20):
                mah_gdt = _calcMahGDT(x[mask], centroid[mask],
                        weights[mask], x, centroid, weights)

                d = _rotatedDistances(x[mask], y[mask], x, y)

                score = np.sum(1 / (1 + (d / d0) ** 2)) / L

                # normal GDT. if distance is less than {1,2,4,8}, add to GDT sum
                gdt1248 = _find_max(d, gdt1248)

                # if flexscore is less than {1,2,4,8}, add to GDT sum
                fsgdt1248 = _find_max(mah_gdt, fsgdt1248)

                # if distance is less than {1,2,4,8}*std_dev, add to GDT sum
                fsgdt1248_sigma = _find_max(d, fsgdt1248_sigma, inv_weights)

                if score > best:
                    best = score

                mask_prev = mask
                cutoff = d0_search + (-1 if i == 0 else 1)
                while True:
                    mask = d < cutoff
                    if np.sum(mask) >= 3 or 3 >= len(mask):
                        break
                    cutoff += 0.5

                if (mask == mask_prev).all():
                    break

    return {'FS_GDT_TS': np.sum(fsgdt1248)/(4.0*L), 'GDT_TS': np.sum(gdt1248)/(4.0*L),
            'tmscore': best, 'FS_GDT_TS_sigma': np.sum(fsgdt1248_sigma)/(4.0*L)}

# utils here
#

class parameters(ctypes.Structure):
    _fields_ = [('x', ctypes.c_double), ('y', ctypes.c_double),
                ('z', ctypes.c_double), ('w', ctypes.c_double)]


def readParameters(filename):
    '''
        Reads theseus output parameters of Gaussian native ensemble model.

        These parameters might be parsed from theseus_transf.txt (centroid)
        and theseus_variances.txt (variance: beta == 1/sigma^2 == weight),
        but using binary out's from theseus, we have double precision
    '''
    with open(filename, 'rb') as f:
        centroid = []  # \hat{M}
        weights = []   # \Sigma^{-1} with 0's off-diagonal
        x = parameters()
        while f.readinto(x) == ctypes.sizeof(x):
            centroid.append((x.x, x.y, x.z))
            weights.append(x.w)
    return np.array(centroid), np.array(weights)


def readTxtParameters(filename_var, filename_cen):
    # RES 1           HIS    529     3.644204     1.908980     5.399410 CORE
    # LM     1 M:     9.733341014724    -8.802689621398   -10.679350577397

    with open(filename_var) as fv:
        var_re2 = re.compile(r'^RES.{23}(?P<var>.{13})\s+.*$', re.M)
        weights = 1./np.array(var_re2.findall(fv.read()), dtype=np.float32)
        # test for RMSD
        # weights = [1 for i in weights]
    with open(filename_cen) as fc:
        cen_re = re.compile(r"^LM\s+\d+\s+M:(?P<x>.{19})(?P<y>.{19})(?P<z>.{19}).*$", re.M)
        centroid = np.array(cen_re.findall(fc.read()), dtype=np.float32)

    return centroid, weights


def readCoordinates(filename):
    '''
        Simple parser of PDB files. It ignores many exotic PDB format things,
        but should work for most cases.
    '''
    atm2 = re.compile(r"^ATOM.{9}CA..(?P<seqid>.{3})..(?P<resid>.{4})"
                     "(?P<x>.{12})(?P<y>.{8})(?P<z>.{8})", re.M)
    with open(filename, "r") as f:
        i = np.array(atm2.findall(f.read()))
        resids = i[:,1]
        seqids = i[:,0]
        structure = np.array(i[:,2:5], dtype=np.float32)
        return structure, resids, seqids

def _savePdb(filename, seq, res, structure):
    with open(filename, "w") as fw:
        for i in range(res.shape[0]):
            l = ("ATOM   %4s  CA  %3s A%4s    %8.3f%8.3f%8.3f"
                    "  1.00  0.00           C\n")
            fw.write(l % (res[i], seq[i], res[i], structure[i][0], structure[i][1],
                            structure[i][2]))

def _saveScore(filename, seq, res, scores):
    l = "%4s %3s %8.2f\n"
    path, fn = os.path.split(filename)
    out = os.path.join(path, "SCO_"+fn)
    with open(out, "w") as fw:
        for i in range(len(res)):
            fw.write(l % (res[i], seq[i], scores[i]))

def _calcMahGDT(s_mask, c_mask, w_mask, whole_structure,
        whole_centroid, whole_w):

    w_mask = w_mask/np.sum(w_mask)
    t3 = np.average(c_mask, axis=0, weights=w_mask)
    t4 = np.average(s_mask, axis=0, weights=w_mask)

    wc = whole_centroid - t3
    ws = whole_structure - t4

    sub_c = c_mask - t3
    sub_s = s_mask - t4

    U, _, V = np.linalg.svd(np.dot(sub_c.T*w_mask, sub_s))

    if np.linalg.det(U) * np.linalg.det(V) < 0:
        P = np.diag([1, 1, -1])
    else:
        P = np.diag([1, 1, 1])

    vp = np.dot(np.dot(V.T, P), U.T)

    rotated = np.dot(ws, vp)

    dists = _distance_sq(rotated, wc)
    z_vals = dists * whole_w

    return np.sqrt(z_vals)

# keep smiling, hacker! :-)
#                 _
#                ( `.
# ,.--.           '. \
#//    \            \ \
#\\    /,.----------.\ \
# `'--'//            \' .
# ,.--.\\            /| |
#//    \`'----------' ' '
#\\    /             / /
# `'--'             / /
#                 .' /
#                (_.'

def _tm_d0(Lmin):

    if Lmin > 15:
        d0 = 1.24 * np.power(Lmin - 15.0, 1.0 / 3.0) - 1.8
    else:
        d0 = 0.5

    return max(0.5, d0)

def _find_max(d, gdt1248, std_dev=None):

    i = 0
    for cutoff in [1, 2, 4, 8]:
        if std_dev is not None:
            count = (d<=cutoff*std_dev).sum()
        else:
            count = (d<=cutoff).sum()

        if count > gdt1248[i]:
            gdt1248[i] = count
        i += 1
    return gdt1248

def _distance(m1, m2):
    u = np.subtract(m1,  m2)
    return np.sqrt((u*u).sum(axis=1))

def _distance_sq(m1, m2):
    u = np.subtract(m1, m2)
    return (u*u).sum(axis=1)

def _rotatedDistances(c_mask, s_mask, whole_centroid, whole_structure):

    t3 = np.average(c_mask, axis=0)
    t4 = np.average(s_mask, axis=0)

    wc = whole_centroid - t3
    ws = whole_structure - t4

    sub_c = c_mask - t3
    sub_s = s_mask - t4

    U, _, V = np.linalg.svd(np.dot(sub_c.T, sub_s))

    if np.linalg.det(U) * np.linalg.det(V) < 0:
        P = np.diag([1, 1, -1])
    else:
        P = np.diag([1, 1, 1])

    vp = np.dot(np.dot(V.T, P), U.T)

    return _distance(np.dot(ws, vp), wc)



def _parseArgs():
    parser = argparse.ArgumentParser(description='FlexScore: compare ' +
                                     'structure with the native-state ' +
                                     'ensemble')
    parser.add_argument('-s', action='store_true', default=False,
                        help='Save superimposed structures onto native state \
                              ensemble Gaussian model (use theseus -Z if you\'re \
                              planning to display it together with theseus_sup.pdb)')
    parser.add_argument('-r', action='store_true', default=False,
                        help='Save score = f(residue index) to SCO_filename')
    parser.add_argument('model', nargs='*', help='PDB file format model')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-p', metavar=('flexscore_input.dat'), nargs=1,
                       help=('native-state ensemble model parameters (defaults: \
                              flexscore_input.dat)'))
    group.add_argument('-t', metavar=('theseus_variances.txt', 'theseus_transf.txt'),
                       nargs=2, help=('native-state ensemble model parameters \
                                       from (theseus -Z ensemble.pdb) output \
                                       [first file must be *_variances.txt, \
                                       second - *_transf.txt]'))

    parser.add_argument('-g', action='store_true', default=False,
                        help='Calculate FlexScore using GDT_TS algorithm and \
                                1,2,4,8[A] cutoffs or using GDT_TS algorithm \
                                and {1,2,4,8}σ cutoffs')

    return parser.parse_args()

# end utils



if __name__ == '__main__':

    args = _parseArgs()

    if args.p is not None:
        c, w = readParameters(args.p[0])
    else:
        c, w = readTxtParameters(args.t[0], args.t[1])
    # calc score for each of model
    for f in args.model:
        if args.s:
            path, fn = os.path.split(f)
            out = os.path.join(path, "ROT_"+fn)
            score = calcMah(f, c, w, output=out, score_res=args.r)
        else:
            score = calcMah(f, c, w, score_res=args.r)
        if args.g:
            gdtts = calcMahWithGDT(f, c, w)
            gdt_ts_fs = gdtts['FS_GDT_TS']
            gdt_ts_sigma = gdtts['FS_GDT_TS_sigma']
            print("flexScore %5.2f GDT_TS-flexScore %6.4f GDT_TS_σ-flexScore \
                    %6.4f %s" % (score, gdt_ts_fs, gdt_ts_sigma, f))
        else:
            print("%5.2f %s" % (score, f))
