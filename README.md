# FlexScore

Program for calculating dissimilarity score between protein structural model and near-native ensemble.


## HowTo

#### Input
~~~~
       ensemble.pdb          :   multimodel PDB file of structural ensemble of 
                                 the native state (i.e. MD trajectory or NMR 
                                 multimodel PDB file)
       model1.pdb model2.pdb :   models to validate
~~~~

#### Usage
1.  Run `theseus -Z ensemble.pdb`
1.  Run `flexscore.py -t theseus_variances.txt theseus_transf.txt model1.pdb model2.pdb` to get flexscore.

Alternatively, use patch from theseus_patch directory (it saves model
parameters in a binary file and skip principal axis aligning (-Z option), compile and:

1.  Run `theseus ensemble.pdb`
1.  Run `flexscore.py -p flexscore_input.dat model1.pdb model2.pdb`

This way of computing flexscore may be slightly more accurate (not tested).

**Other options:**
~~~~
  -s                    Save superimposed structures onto native state ensemble
                        Gaussian model. 
~~~~
**use `theseus -Z` if you're planning to display structure superiposed by FlexScore onto the ensemble file (theseus_sup.pdb)!**
~~~~
  -r                    Save score = f(residue index) to `SCO_filename`
~~~~
GDT TS substructures superimposition search is possible with `-g` option. Script
computes two types of scores:

1. where each residue flexScore Z-score fit below GDT cutoffs (1, 2, 4, and 8 Angstrom), 
1. where each residue distance is closer to the ensemble centroid than 1, 2, 4, and 8 standard deviations (σ).


**All options:** `flexscore.py -h`:

~~~~
usage: flexscore.py [-h] [-s] [-r]
                    (-p flexscore_input.dat | -t theseus_variances.txt theseus_transf.txt)
                    [-g]
                    [model [model ...]]

FlexScore: compare structure with the native-state ensemble

positional arguments:
  model                 PDB file format model

optional arguments:
  -h, --help            show this help message and exit
  -s                    Save superimposed structures onto native state
                        ensemble Gaussian model (use theseus -Z if you're
                        planning to display it together with theseus_sup.pdb)
  -r                    Save score = f(residue index) to SCO_filename
  -p flexscore_input.dat
                        native-state ensemble model parameters (defaults:
                        flexscore_input.dat)
  -t theseus_variances.txt theseus_transf.txt
                        native-state ensemble model parameters from (theseus
                        -Z ensemble.pdb) output [first file must be
                        *_variances.txt, second - *_transf.txt]
  -g                    Calculate FlexScore using GDT_TS algorithm and
                        1,2,4,8[A] cutoffs or using GDT_TS algorithm and
                        {1,2,4,8}σ cutoffs
~~~~

## TEST
~~~~
$ cd example
$ ../flexscore.py  -t theseus_variances.txt theseus_transf.txt  2*
1.73 2j8p.pdb10
1.06 2j8p.pdb11
1.75 2j8p.pdb12
2.46 2j8p.pdb13

$ cd example/toy_model/
$ cat run.sh
#!/usr/bin/env bash
../../flexscore.py -g -t theseus_variances.txt theseus_transf.txt m*pdb
$ ./run.sh 
flexScore  1.42 GDT_TS-flexScore 0.8061 GDT_TS_σ-flexScore 0.7959 modified_flexible_end.pdb
flexScore  1.97 GDT_TS-flexScore 0.7347 GDT_TS_σ-flexScore 0.7194 modified_helix.pdb
~~~~
