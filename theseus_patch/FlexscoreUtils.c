/*
*  File:           FlexscoreUtils.c
*
*  Function:       Get float precision values of mean structure 
*                  and variances from THESEUS
*
*  Author(s):      Michal Jamroz
*  Copyright:      Copyright (c) 2014 Michal Jamroz
*
*  You should have received a copy of the GNU General Public
*  License along with this file in the file 'COPYING'; if not, write
*  to the:
*
*  Free Software Foundation, Inc.,
*  59 Temple Place, Suite 330,
*  Boston, MA  02111-1307  USA
*/

#include "FlexscoreUtils.h"
typedef struct { double x; double y; double z; double w;} line_out;

void saveParamsMJ(CdsArray *cdsA) {
    FILE *f = fopen("flexscore_input.dat", "w");
    for (int i = 0; i < cdsA->vlen; i++) {
        line_out l = { cdsA->avecds->x[i], cdsA->avecds->y[i], cdsA->avecds->z[i], cdsA->w[i]};
        fwrite(&l, sizeof(line_out), 1, f);
        printf("%8.4f\n",cdsA->w[i]);

    }
    fclose(f);
}
