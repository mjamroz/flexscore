/*******************************************************************
* THIS IS PATCHED FILE, NOT THE ORIGINAL ONE FROM theseus3d.org
*
*  -/_|:|_|_\-
*
*  File:           theseus.c
*
*  Function:       THESEUS: Maximum likelihood superpositioning of
*                  multiple macromolecular structures
*
*  Patch by Michal Jamroz
*
*  Author(s) of the original code:      
*                  Douglas L. Theobald
*                  Biochemistry Department
*                  Brandeis University
*                  MS 009
*                  415 South St
*                  Waltham, MA  02454-9110
*
*                  dtheobald@gmail.com
*                  dtheobald@brandeis.edu
*
*  Copyright:      Copyright (c) 2004-2014 Douglas L. Theobald
*
*  THESEUS is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as
*  published by the Free Software Foundation; either version 2 of
*  the License, or (at your option) any later version.
*
*  THESEUS is distributed in the hope that it will be useful, but
*  WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with THESEUS in the file 'COPYING'; if not, write
*  to the:
*
*  Free Software Foundation, Inc.,
*  59 Temple Place, Suite 330,
*  Boston, MA  02111-1307  USA
*
*  Source:         started anew.
*
*  Notes:
*
*  Change History:
*          9/6/04 3:24 PM   Started source
*
*  -/_|:|_|_\-
*
******************************************************************/
#include "theseus.h"
#include "theseuslib.h"


void
leave(int sig);

/* global declarations (necessary for leave(), I think) */
const gsl_rng_type     *T = NULL;
gsl_rng                *r2 = NULL;

CdsArray       *cdsA = NULL; /* main array of selected pdb cds, never modified */
PDBCdsArray    *pdbA = NULL; /* pdb file coordinate info, much of it unused */
Algorithm      *algo = NULL;
Statistics     *stats = NULL;


/*
void
testcrush(CdsArray *cdsA)
{
    int         i, j, m, n;
    double      lndistsqr;

    for (i = 0; i < cdsA->cnum; ++i)
    {
        for (j = 0; j < i; ++j)
        {
            for (m = 0; m < cdsA->cds[i]->vlen; ++m)
                for (n = 0; n < cdsA->cds[j]->vlen; ++n)
                    lndistsqr = log(SqrCdsDist(cdsA->cds[i], m, cdsA->cds[j], n));
        }
    }
}
*/


static void
RotPrincAxes(CdsArray *cdsA)
{
    int             i;
//     double        **x90z90 = MatAlloc(3,3);
    /* double x90[3][3]    = {{ 1, 0, 0}, { 0, 0, 1}, { 0,-1, 0}}; */
    /* double z90[3][3]    = {{ 0, 1, 0}, {-1, 0, 0}, { 0, 0, 1}}; */
    /* double x90z90[3][3] = {{ 0, 1, 0}, { 0, 0, 1}, { 1, 0, 0}}; */

    /* this orients the least -> most variable axes along x, y, z respectively (??) */
    CalcCdsPrincAxes(cdsA->avecds, cdsA->avecds->matrix, cdsA->tmpmat3a, cdsA->tmpmat3b, cdsA->tmpvec3a, cdsA->w);

//     memset(&x90z90[0][0], 0, 9 * sizeof(double));
//     x90z90[0][1] = x90z90[1][2] = x90z90[2][0] = 1.0;
//
//     /* Rotate the family 90deg along x and then along z.
//        This puts the most variable axis horizontal, the second most variable
//        axis vertical, and the least variable in/out of screen. */
//     Mat3MultIp(cdsA->avecds->matrix, (const double **) x90z90);

    for (i = 0; i < cdsA->cnum; ++i)
        Mat3MultIp(cdsA->cds[i]->matrix, (const double **) cdsA->avecds->matrix);

//     MatDestroy(&x90z90);
}


static double
SuperPoseCds(double **c1, double **c2, const int *nu, const int vlen,
             double **rotmat, double *trans,
             double *norm1, double *norm2, double *innprod)
{
    double        **tmpmat1 = MatAlloc(3, 3);
    double        **tmpmat2 = MatAlloc(3, 3);
    double        **tmpmat3 = MatAlloc(3, 3);
    double         *tmpvec = malloc(3 * sizeof(double));
    double         *newtrans = malloc(3 * sizeof(double));
    double         *cen1 = calloc(3, sizeof(double));
    double         *cen2 = calloc(3, sizeof(double));
    double          sumdev;
    int             i;

    CenMassNuVec((const double **) c1, nu, cen1, vlen);
    CenMassNuVec((const double **) c2, nu, cen2, vlen);

    NegTransCdsIp(c1, cen1, vlen);
    NegTransCdsIp(c2, cen2, vlen);

    sumdev = ProcGSLSVDvanNu2((const double **) c1, (const double **) c2, nu,
                               vlen, rotmat,
                               tmpmat1, tmpmat2, tmpmat3, tmpvec,
                               norm1, norm2, innprod);

    TransCdsIp(c1, cen1, vlen);
    TransCdsIp(c2, cen2, vlen);

    InvRotVec(newtrans, cen2, rotmat);

    for (i = 0; i < 3; ++i)
        trans[i] = newtrans[i] - cen1[i];

    MatDestroy(&tmpmat1);
    MatDestroy(&tmpmat2);
    MatDestroy(&tmpmat3);
    free(tmpvec);
    free(newtrans);
    free(cen1);
    free(cen2);

    return(sumdev);
}


static double
SuperPose2Anchor(CdsArray *cdsA, char *anchorf_name)
{
    double        **anchormat = MatAlloc(3, 3);
    double         *anchortrans = malloc(3 * sizeof(double));
    double         *tmpanchortrans = malloc(3 * sizeof(double));
    double         *trans = malloc(3 * sizeof(double));
    double          norm1, norm2, innprod, sumdev;
    const int       cnum = cdsA->cnum;
    int             i, j, anchor = 0;

    for (i = 0; i < cnum; ++i)
    {
        if (strncmp(anchorf_name, cdsA->cds[i]->filename, FILENAME_MAX - 1) == 0)
        {
            anchor = i;
            break;
        }
    }

    sumdev = SuperPoseCds(cdsA->cds[anchor]->wc, cdsA->cds[anchor]->sc,
                          cdsA->cds[anchor]->nu, cdsA->vlen,
                          anchormat, anchortrans,
                          &norm1, &norm2, &innprod);

    for (i = 0; i < cnum; ++i)
    {
        InvRotVec(tmpanchortrans, anchortrans, cdsA->cds[i]->matrix);

        for (j = 0; j < 3; ++j)
            cdsA->cds[i]->center[j] = cdsA->cds[i]->translation[j] =
                cdsA->cds[i]->center[j] - tmpanchortrans[j];

        Mat3MultIp(cdsA->cds[i]->matrix, (const double **) anchormat);
    }

    for (j = 0; j < 3; ++j)
        cdsA->avecds->center[j] = cdsA->avecds->translation[j] =
            anchortrans[j];

    Mat3Cpy(cdsA->avecds->matrix, (const double **) anchormat);

    free(trans);
    free(anchortrans);
    free(tmpanchortrans);
    MatDestroy(&anchormat);

    return(sumdev);
}


int
main(int argc, char *argv[])
{
    int             i = 0, j;
    char           *sup_name = NULL, *ave_name = NULL, *transf_name = NULL,
                   *rand_transf_name = NULL, *mean_ip_name = NULL, *sup_var_name = NULL,
                   *tps_sup_name = NULL, *tps_ave_name = NULL;

    int             cnum;

    clock_t         start_time, end_time;

    signal(SIGINT, leave);
    signal(SIGABRT, leave);

//    init_genrand(seed);

/*    clrscr(); */
/*     #define CHMOD744 (S_IRWXU | S_IROTH | S_IRGRP) */
/*     mkdir("doug", CHMOD744); */

//#ifndef __MINGW32__
//    if (setrlimit(RLIMIT_CORE, 0) == 0)
//        printf("\n  WARNING2: could not set core limit size to 0. \n\n");
//#endif

    cdsA = CdsArrayInit();
    algo = AlgorithmInit();
    stats = StatsInit();

    ParseCmdLine(argc, argv, cdsA);
    algo->infiles = &argv[optind];
    algo->filenum = argc - optind;

    if (algo->random == 'h')
    {
        RandUsage();
        exit(EXIT_FAILURE);
    }

    if (algo->filenum < 1)
    {
        printf("\n  -> No pdb files specified. <- \n");
        Usage(0);
        exit(EXIT_FAILURE);
    }

    PrintTheseusPre();

    if (algo->convlele > 0)
    {
//         ConvertDryden(algo->infiles[0], 2,
//                       algo->iterations, /* # coordinates/forms */
//                       algo->landmarks /* # of landmarks */);
        ConvertLele_freeform(algo->infiles[0], 3,
                             algo->iterations, /* # coordinates/forms */
                             algo->landmarks /* # of landmarks */);
        PrintTheseusTag();
        exit(EXIT_SUCCESS);
    }

    // for benchmarking, testing ML algorithm
    if (/* algo->random > */ 0)
    {
        gsl_rng_env_setup();
        gsl_rng_default_seed = time(NULL) + getpid() + clock();
        T = gsl_rng_ranlxs2;
        r2 = gsl_rng_alloc(T);

        if (algo->random != '0')
        {
            strcat(algo->rootname, "_rand");
            //printf("\nfmodel\n = %d", algo->fmodel);
            pdbA = GetPDBCds(algo->infiles, algo->filenum, algo->fmodel, algo->amber, algo->atom_names);
//             if (algo->fmodel)
//             {
//                 pdbA = MakeRandPDBCds(algo->iterations, /* # coordinates/forms */
//                                          algo->landmarks, /* # of landmarks */
//                                          &algo->radii[0], r2);
//             }
//             else
//             {
//                 pdbA = GetRandPDBCds(algo->infiles[0], algo->iterations /* # coordinates/forms */, algo->landmarks /* # of landmarks */);
//             }

            pdbA->vlen = pdbA->cds[0]->vlen;
            PDBCdsArrayAllocLen(pdbA, pdbA->vlen);
            cnum = pdbA->cnum;
            GetCdsSelection(cdsA, pdbA);

            //printf("HERE2\n\n"); fflush(NULL);

            RandCds_2sdf(cdsA, r2);
            memcpy(cdsA->avecds->resSeq, cdsA->cds[0]->resSeq, cdsA->vlen * sizeof(int));
            memcpy(cdsA->avecds->chainID, cdsA->cds[0]->chainID, cdsA->vlen * sizeof(char));
            memcpy(cdsA->avecds->resName_space, cdsA->cds[0]->resName_space, cdsA->vlen * 4 * sizeof(char));
            AveCds(cdsA);

            for (i = 0; i < cnum; ++i)
                CopyCds2PDB(pdbA->cds[i], cdsA->cds[i]);

            printf("    Writing CA coordinates of random structures ... \n");
            fflush(NULL);
            sup_name = mystrcat(algo->rootname, "_sup.pdb");
            WriteTheseusModelFile(pdbA, algo, stats, sup_name);

            CopyCds2PDB(pdbA->avecds, cdsA->avecds);

            printf("    Writing CA coordinates of average of random structures ... \n");
            fflush(NULL);
            ave_name = mystrcat(algo->rootname, "_ave.pdb");
            WriteAvePDBCdsFile(pdbA, ave_name);

            printf("    Calculating statistics of random structures ... \n");
            fflush(NULL);

            CalcPreStats(cdsA);
            PrintSuperposStats(cdsA);
        }

        RandRotCdsArray(cdsA, r2);
        RandTransCdsArray(cdsA, 0.004, r2);
        printf("    Writing CA coordinates of transformed random structures ... \n");
        fflush(NULL);

        for (i = 0; i < cnum; ++i)
            CopyCds2PDB(pdbA->cds[i], cdsA->cds[i]);

        transf_name = mystrcat(algo->rootname, "_transf.pdb");
        WriteTheseusModelFile(pdbA, algo, stats, transf_name);
        // WriteLeleModelFile(pdbA);

        PrintTheseusTag();

//         CdsArrayDestroy(&cdsA);
//         PDBCdsArrayDestroy(&pdbA);

        exit(EXIT_SUCCESS);
    }

    /* read the PDB files, putting full cds in pdbA */
    if (algo->binary == 2 || algo->binary == 4)
    {
        printf("    Reading binary pdb file ... \n");
        fflush(NULL);

        pdbA = ReadBinPDBCdsArray(algo->infiles[0]);
    }
    else if (algo->morphfile)
    {
        if (algo->filenum)
            printf("    Reading tps file ... \n");
        else
            printf("    Reading %d tps files ... \n", algo->filenum);
        fflush(NULL);

        pdbA = GetTPSCds(algo->infiles, algo->filenum);
    }
    else
    {
        if (algo->filenum)
            printf("    Reading pdb file ... \n");
        else
            printf("    Reading %d pdb files ... \n", algo->filenum);
        fflush(NULL);

        pdbA = GetPDBCds(algo->infiles, algo->filenum, algo->fmodel, algo->amber, algo->atom_names);
        /* PrintPDBCds(stdout, pdbA->cds[0]); */
    }

    cnum = pdbA->cnum;
    if (algo->fasta)
    {
        if (cnum < 1)
        {
            printf("\n  -> Found no PDB cds. Could not determine a sequence. <- \n");
            Usage(0);
            exit(EXIT_FAILURE);
        }

        printf("    Writing FASTA format .fst files (%d) ... \n", cnum);
        pdb2fst(pdbA);
        PrintTheseusTag();
        exit(EXIT_SUCCESS);
    }

    if (cnum < 2)
    {
        printf("\n  -> Found less than two PDB cds. Could not do superposition. <- \n");
        Usage(0);
        exit(EXIT_FAILURE);
    }

    printf("    Successfully read %d models and/or structures \n", cnum);

    if (algo->binary)
    {
        printf("    Writing binary coordinates file ... \n");
        fflush(NULL);

        WriteBinPDBCdsArray(pdbA);
        PrintTheseusTag();
        exit(EXIT_SUCCESS);
    }
    else if (algo->alignment)
    {
        printf("    Reading multiple sequence alignment ... \n");
        fflush(NULL);

        Align2MSA(pdbA, cdsA, cdsA->msafile_name, cdsA->mapfile_name);
        PDBCdsArrayAllocLen(pdbA, cdsA->vlen);
    }
    else
    {
        printf("    Selecting coordinates for superposition ... \n");
        fflush(NULL);

        pdbA->vlen = NMRCheckPDBCdsArray(pdbA);
        PDBCdsArrayAllocLen(pdbA, pdbA->vlen);
        GetCdsSelection(cdsA, pdbA);
    }

    cdsA->pdbA = pdbA;
    pdbA->cdsA = cdsA;

    if (algo->scalefactor > 1.0 || algo->scalefactor < 1.0)
    {
        for (i = 0; i < cnum; ++i)
            ScaleCds(cdsA->cds[i], algo->scalefactor);
    }

    /* CalcPreStats(cdsA); */

    if (algo->random == '0')
    {
        gsl_rng_env_setup();
        gsl_rng_default_seed = time(NULL) + getpid() + clock();
        T = gsl_rng_ranlxs2;
        r2 = gsl_rng_alloc(T);

        RandRotCdsArray(cdsA, r2);
        RandTransCdsArray(cdsA, 300, r2);
        printf("    Writing CA coordinates of transformed random structures ... \n");
        fflush(NULL);

        for (i = 0; i < cnum; ++i)
            CopyCds2PDB(pdbA->cds[i], cdsA->cds[i]);

        rand_transf_name = mystrcat(algo->rootname, "_rand_transf.pdb");
        WriteTheseusModelFile(pdbA, algo, stats, rand_transf_name);
    }

    if (algo->FragDist > 0)
    {
        printf("    FUN!!!!! %d \n", algo->FragDist);
        fflush(NULL);
        FragDistPu(cdsA, algo->FragDist, 2, algo->pu);
    }
    else if (algo->info)
    {
        printf("    Calculating superposition statistics ... \n");
        fflush(NULL);

        memsetd(cdsA->w, 1.0, cdsA->vlen);
        algo->rounds = 100;

        if (algo->covweight)
        {
            SetupCovWeighting(cdsA);
            memsetd(cdsA->evals, 1.0, cdsA->vlen);
        }

        CalcStats(cdsA);
    }
    else if (algo->mixture > 1)
    {
        printf("    Calculating mixture superposition transformations ... \n");
        fflush(NULL);

        if (algo->threads > 0)
        {
            printf("    Using %d threads ... \n", algo->threads);
            fflush(NULL);
            Mixture_pth(cdsA, pdbA);
        }
        else
        {
            Mixture(cdsA, pdbA);
        }
    }
    else if (algo->bayes > 0)
    {
        printf("    Calculating Gibbs-Metropolis Bayesian superposition ... \n");
        fflush(NULL);

        InitializeStates(cdsA);

        if (algo->domp)
            MultiPose(cdsA);

        GibbsMet(cdsA);
    }
    else
    {
        printf("    Calculating superposition transformations ... \n");
        fflush(NULL);

        start_time = clock();

        if (algo->threads > 0)
        {
            printf("    Using %d threads ... \n", algo->threads);
            fflush(NULL);
            //MultiPose_pth(cdsA);
            InitializeStates(cdsA);
            MultiPoseLib(cdsA); // DLT Broken, needs initialization
            //test_charmm(cdsA);
        }
        else
        {
            InitializeStates(cdsA);
            MultiPose(cdsA);
            //testcrush(cdsA);
        }

        end_time = clock();
        algo->milliseconds = (double) (end_time - start_time) / ((double) CLOCKS_PER_SEC * 0.001);
    }

//     if (algo->scale > 0)
//     {
//         putchar('\n');
//         for (i = 0; i < cnum; ++i)
//             printf("scale[%3d]: %20.8f\n", i, cds[i]->scale / cds[0]->scale);
//         putchar('\n');
//         fflush(NULL);
//     }

        // Jamroz: get weights after ML par. esitmations
        saveParamsMJ(cdsA);

    printf("    Calculating statistics ... \n");
    fflush(NULL);

/*
    fp = fopen("distcor.txt", "w");
    if (CovMat == NULL)
        CovMat = MatAlloc(vlen, vlen);

    CalcCovMat(cdsA);
    DistMatsAlloc(cdsA);

    CalcMLDistMat(cdsA);

    for (i = 0; i < vlen; ++i)
        for (j = 0; j < i; ++j)
            fprintf(fp, "%6d % 10.3f  % 8.3e\n",
                   i-j,
                   cdsA->Dij_matrix[i][j],
                   CovMat[i][j] / sqrt(CovMat[i][i] * CovMat[j][j]));

    fclose(fp);
*/

/*     if (algo->weight == 200) */
/*         unremlvar(cdsA); */

/* #include "internmat.h" */
/*  if (algo->doave) */
/*      AveCds(cdsA); */
/* CalcCovMat(cdsA); */
/* PrintCovMatGnuPlot((const double **) CovMat, vlen, "cov.mat"); */
/* for (i = 0; i < vlen; ++i) */
/*  for (j = 0; j < vlen; ++j) */
/*      CovMat[i][j] -= internmat[i][j]; */
/* PrintCovMatGnuPlot((const double **) CovMat, vlen, "covdiff.mat"); */

/*     CovMat2CorMat(CovMat, vlen); */
/*     PrintCovMatGnuPlot((const double **) CovMat, vlen, "corr.mat"); */
/*     memcpy(&CovMat[0][0], &internmat[0][0], vlen * vlen * sizeof(double)); */
/*     PrintCovMatGnuPlot((const double **) CovMat, vlen, "cov_true.mat"); */
/*     CovMat2CorMat(CovMat, vlen); */
/*     PrintCovMatGnuPlot((const double **) CovMat, vlen, "corr_true.mat"); */

/*     CovMatsDestroy(cdsA); */

    if (algo->covweight && (algo->write_file > 0 || algo->info))
    {
        double         *evals = malloc(cdsA->vlen * sizeof(double));
        char           *mp_cov_name = NULL;

        EigenvalsGSL((const double **) cdsA->CovMat, cdsA->vlen, evals);

        /* VecPrint(evals, vlen); */
        mp_cov_name = mystrcat(algo->rootname, "_mp_cov.mat");
        PrintCovMatGnuPlot((const double **) cdsA->CovMat, cdsA->vlen, mp_cov_name);
        free(mp_cov_name);
/*         CovMat2CorMat(CovMat, vlen); */
/*         PrintCovMatGnuPlot((const double **) CovMat, vlen, mystrcat(algo->rootname, "_cor.mat")); */
        CalcPRMSD(cdsA);
        WriteInstModelFile("_mp.pdb", cdsA);
        free(evals);
    }

    WriteDistMatTree(cdsA);

    CalcStats(cdsA);

    if (algo->ssm)
        WriteEdgarSSM(cdsA);


    if (cdsA->anchorf_name != NULL) /* orient to a user-specified structure */
        SuperPose2Anchor(cdsA, cdsA->anchorf_name);
    //else if (algo->princaxes) /* orient perpendicular to principal axes of mean cds */
        //RotPrincAxes(cdsA);   /* makes for nice viewing */


    if (algo->olve && algo->write_file)
        WriteOlveFiles(cdsA);


    if (algo->bayes == 0)
        PrintSuperposStats(cdsA);

    if (algo->write_file && algo->bayes == 0)
    {
        printf("    Transforming coordinates ... \n");
        fflush(NULL);

        if (algo->atoms == 2) /* 2 = all atoms */
        {
            for (j = 0; j < cnum; ++j)
                memcpy(pdbA->cds[j]->tempFactor, cdsA->avecds->b, cdsA->vlen * sizeof(double));
        }

        for (i = 0; i < cnum; ++i)
        {
            Mat3Cpy(pdbA->cds[i]->matrix, (const double **) cdsA->cds[i]->matrix);
            memcpy(pdbA->cds[i]->translation, cdsA->cds[i]->translation, 3 * sizeof(double));
            pdbA->cds[i]->scale = cdsA->cds[i]->scale;
        }

        Mat3Cpy(pdbA->avecds->matrix, (const double **) cdsA->avecds->matrix);
        memcpy(pdbA->avecds->translation, cdsA->avecds->translation, 3 * sizeof(double));
        for (i = 0; i < cnum; ++i) 
            TransformPDBCdsIp(pdbA->cds[i]);

        transf_name = mystrcat(algo->rootname, "_transf.txt");
        WriteTransformations(cdsA, transf_name);

        if (algo->fullpca && algo->alignment == 0)
        {
            printf("    Writing anisotropic Principal Component coordinate files ... \n");
            fflush(NULL);

            if (algo->morph)
                WritePCAMorphFile(pdbA, cdsA, algo->rootname);
            else
                WritePCAProjections(pdbA, cdsA, algo->rootname);
        }
        else if (algo->pca > 0 && algo->alignment == 0)
        {
            printf("    Writing isotropic Principal Component coordinate files ... \n");
            fflush(NULL);
            WritePCAFile(pdbA, cdsA, algo->rootname);
        }

        if (algo->alignment)
        {
            Align2segID(pdbA);

            if (algo->olve > 0)
            {
                double          olve;
                for (i = 0; i < cnum; ++i)
                {
                    for (j = 0; j < pdbA->cds[i]->vlen; ++j)
                    {
                        sscanf(pdbA->cds[i]->segID[j], "%4lf", &olve);
                        pdbA->cds[i]->tempFactor[j] = olve / 100.0;
                    }
                }
            }
        }

        printf("    Writing transformed coordinates PDB file ... \n");
        fflush(NULL);

        sup_name = mystrcat(algo->rootname, "_sup.pdb");
        WriteTheseusModelFile(pdbA, algo, stats, sup_name);

        if (algo->morphfile)
        {
            tps_sup_name = mystrcat(algo->rootname, "_sup.tps");
            WriteTheseusTPSModelFile(pdbA, tps_sup_name);
        }

        if (algo->alignment)
            WriteTheseusPDBFiles(pdbA, algo, stats);

        if (algo->binary == 3 || algo->binary == 4)
        {
            printf("    Writing transformed coordinates binary file ... \n");
            fflush(NULL);

            WriteBinPDBCdsArray(pdbA);
        }

        printf("    Writing average coordinate file ... \n");
        fflush(NULL);

        TransformCdsIp(cdsA->avecds);
        CopyCds2PDB(pdbA->avecds, cdsA->avecds);
        ave_name = mystrcat(algo->rootname, "_ave.pdb");
        WriteAvePDBCdsFile(pdbA, ave_name);

        if (algo->morphfile)
        {
            tps_ave_name = mystrcat(algo->rootname, "_ave.tps");
            WriteAveTPSCdsFile(pdbA, tps_ave_name);
        }

        if (cdsA->avecds->outerprod == NULL)
            cdsA->avecds->outerprod = MatAlloc(cdsA->vlen, cdsA->vlen);

        if (algo->ipmat)
        {
            printf("    Writing mean inner product file ... \n");
            fflush(NULL);

            CenMass(cdsA->avecds);
            ApplyCenterIp(cdsA->avecds);
            CdsInnerProd(cdsA->avecds);
            mean_ip_name = mystrcat(algo->rootname, "_mean_ip.mat");
            PrintCovMatGnuPlot((const double **) cdsA->avecds->outerprod, cdsA->vlen, mean_ip_name);
        }

        if (algo->alignment)
        {
            sup_var_name = mystrcat(algo->rootname, "_sup_var.pdb");
            Align2Vars(pdbA, cdsA);
            WriteTheseusModelFile(pdbA, algo, stats, sup_var_name);
            strcat(algo->rootname, "_var");
            WriteTheseusPDBFiles(pdbA, algo, stats);
        }
    }

    PrintTheseusTag();

    AlgorithmDestroy(algo);
    free(stats);
    stats = NULL;

    CdsArrayDestroy(&cdsA);
    PDBCdsArrayDestroy(&pdbA);

    if (sup_name != NULL)
        free(sup_name);
    if (sup_var_name != NULL)
        free(sup_var_name);
    if (ave_name != NULL)
        free(ave_name);
    if (transf_name != NULL)
        free(transf_name);
    if (rand_transf_name != NULL)
        free(rand_transf_name);
    if (mean_ip_name != NULL)
        free(mean_ip_name);

    exit(EXIT_SUCCESS);
}


static void
ParseCmdLine(int argc, char *argv[], CdsArray *bseA)
{
    int            option;
    extern char   *optarg;
    extern int     optind, optopt;
    int            option_index = 0;
    int            i, cmdlinelen, argvlen;
    char           space[] = " ";

    /* save the command line, both in parsed form and as a whole string */
    algo->argc = argc;
    algo->argv = malloc(argc * sizeof(char *));

    algo->cmdline[0] = '\0';

    cmdlinelen = 0;
    for (i = 0; i < argc; ++i)
    {
        algo->argv[i] = NULL;
        argvlen = strlen(argv[i]);
        cmdlinelen += argvlen + 1;
        if (cmdlinelen >= 1024)
            break;
        strncat(algo->cmdline, argv[i], argvlen);
        strncat(algo->cmdline, space, 1);
        algo->argv[i] = malloc((argvlen+1) * sizeof(char));
        strncpy(algo->argv[i], argv[i], argvlen);
    }

    char short_options[] = "a:A:b:B:cCd:D:e:EfFg:GhHi:Ij:Jk:K:lLM:nNo:Op:P:qQ:r:R:s:S:T:uUvVw:WxXyYz:Z0123:4:69";

    struct option long_options[] =
    {
        {"alignment",    required_argument, 0, 'A'},
        {"amber",        no_argument,       0,  0 },
        {"bayes",        required_argument, 0, 'b'},
        {"covariance",   no_argument,       0, 'c'}, // aliased to the short_option 'c'
        {"fasta",        no_argument,       0, 'F'},
        {"help",         no_argument,       0, 'h'},
        {"info",         no_argument,       0, 'I'},
        {"invselection", required_argument, 0, 'S'},
        {"iterations",   required_argument, 0, 'i'},
        {"leastsquares", no_argument,       0, 'l'},
        {"mapfile",      required_argument, 0, 'M'},
        {"morphfile",    no_argument,       0, 'q'},
        {"noave"  ,      no_argument,       0, 'y'},
        {"nomp"   ,      no_argument,       0,  0 },
        {"notrans",      no_argument,       0, '0'},
        {"norot",        no_argument,       0, '1'},
        {"nohierarch",   no_argument,       0,  0 },
        {"nocovars",     no_argument,       0,  0 },
        {"orient",       required_argument, 0, 'o'},
        {"pca",          required_argument, 0, 'P'},
        {"precision",    required_argument, 0, 'p'},
        {"randgibbs",    no_argument,       0,  0 },
        {"rootname",     required_argument, 0, 'r'},
        {"scaleanchor",  required_argument, 0,  0 },
        {"seed",         required_argument, 0, 'X'},
        {"selection",    required_argument, 0, 's'},
        {"verbose",      optional_argument, 0, 'W'},
        {"version",      no_argument,       0, 'V'},
        {0,              0,                 0,  0 } // required zero array
    };

    while ((option = getopt_long(argc, argv, short_options, long_options, &option_index)) != -1)
    {
        switch (option) /* See Algorithm structure in Cds.h for explanations of these flags/options */
        {
            case 0:
                if (strcmp(long_options[option_index].name, "notrans") == 0)
                {
                    algo->dotrans = 0;
                }
                else if (strcmp(long_options[option_index].name, "nohierarch") == 0)
                {
                    algo->dohierarch = 0;
                }
                else if (strcmp(long_options[option_index].name, "nocovars") == 0)
                {
                    algo->docovars = 0;
                }
                else if (strcmp(long_options[option_index].name, "nomp") == 0)
                {
                    algo->domp = 0;
                }
                else if (strcmp(long_options[option_index].name, "amber") == 0)
                {
                    algo->amber = 1; /* for dealing with fugly AMBER8 PDB format, with atom in wrong column */
                }
                else if (strcmp(long_options[option_index].name, "bayes") == 0)
                {
                    algo->bayes = (int) strtol(optarg, NULL, 10);
                }
                else if (strcmp(long_options[option_index].name, "scaleanchor") == 0)
                {
                    algo->scaleanchor = (int) strtol(optarg, NULL, 10);
                }
                else if (strcmp(long_options[option_index].name, "randgibbs") == 0)
                {
                    algo->randgibbs = 1;
                }
/*
                else
                {
                    printf("\n  Bad option '--%s' \n", long_options[option_index].name);
                    Usage(0);
                    exit(EXIT_FAILURE);
                }
 */
                break;
            case '0': /* don't do translations */
                algo->dotrans = 0;
                break;
            case '1': /* don't estimate rotations */
                algo->dorot = 0;
                break;
            case '2': /* convert a Lele formatted file to PDB */
                algo->convlele = 1;
                break;
            case '3': /* scale and shape params for the inv gamma dist for the random generated variances */
                sscanf(optarg, "%lf:%lf",
                       &algo->param[0],
                       &algo->param[1]);
                break;
            case '4': /* average radius of gyration for the atoms randomly generated (Gaussian) */
                sscanf(optarg, "%lf:%lf:%lf",
                       &algo->radii[0],
                       &algo->radii[1],
                       &algo->radii[2]);
                algo->fmodel = 1;
                break;
            case '6':
                algo->ssm = 1;
                break;
            case '9': /* write out the mean inner product matrix (Lele uses this) */
                algo->ipmat = 1;
                break;
            case 'a':
                if (algo->alignment)
                {
                    printf("\n\n    -> Only alpha carbons can be selected <-");
                    printf("\n    -> when superimposing to an alignment <-\n");
                    algo->atoms = 0;
                }

                if (algo->pca > 0)
                {
                    printf("\n\n    -> Only alpha carbons can be selected <-");
                    printf("\n    -> when calculating principle components <-\n");
                    algo->atoms = 0;
                }

                if (isdigit(optarg[0]))
                {
                    algo->atoms = (int) strtol(optarg, NULL, 10);
                    if (algo->atoms > 4 || algo->atoms < 0)
                    {
                        printf("\n  Bad -a string \"%s\" \n", optarg);
                        Usage(0);
                        exit(EXIT_FAILURE);
                    }
                }
                else
                {
                    strtoupper(optarg);
                    algo->atomslxn = (char *) malloc((strlen(optarg) + 1) * sizeof(char));
                    mystrncpy(algo->atomslxn, optarg, FILENAME_MAX - 1);
                    algo->atoms = 10;
                }
                break;
            case 'A':
                bseA->msafile_name = (char *) malloc((strlen(optarg) + 2) * sizeof(char));
                mystrncpy(bseA->msafile_name, optarg, strlen(optarg) + 1);

                if (algo->atoms > 0)
                {
                    printf("\n\n    -> Only alpha carbons can be selected <-");
                    printf("\n    -> when superimposing to an alignment <-\n");
                    algo->atoms = 0;
                }

                algo->alignment = 1;
                algo->fmodel = 1;
                break;
            case 'b': /* runs GibbsMet() */
                algo->bayes = (int) strtol(optarg, NULL, 10);
                break;
            case 'B': /* 1 = read a binary PDB structure, 2 = write one */
                algo->binary = (int) strtol(optarg, NULL, 10);
                break;
            case 'c':
                algo->covweight = 1;
                algo->varweight = 0;
                algo->leastsquares = 0;
                break;
            case 'C':
                algo->cormat = 0;
                break;
            case 'd':
                 algo->scale = (double) strtod(optarg, NULL); /* specify a type of scaling to do */
                 break;
/*             case 'd':*/ /* FragDist benchmarking, LAPACK rotation calc */
/*                 algo->FragDist = (int) strtol(optarg, NULL, 10); */
/*                 algo->pu = 0; */
/*                 break; */
            case 'D': /* FragDist benchmarking, Pu/Theobald QCP rotation calc */
                algo->FragDist = (int) strtol(optarg, NULL, 10);
                algo->pu = 1;
                break;
            case 'e':
                algo->embedave = (int) strtol(optarg, NULL, 10);
                break;
            case 'E': /* expert options help message */
                Usage(1);
                exit(EXIT_SUCCESS);
                break;
            case 'f':
                algo->fmodel = 1;
                break;
            case 'F':
                algo->fasta = 1;
                break;
            case 'g':
                if (isdigit(optarg[0]))
                    algo->hierarch = (int) strtol(optarg, NULL, 10);
                break;
            case 'G':
                //algo->fullpca = 1;
                // find rotations with QCP algorithm
                algo->pu = 1;
                break;
            case 'h':
                Usage(0);
                exit(EXIT_SUCCESS);
                break;
            case 'H':
                algo->morph = 1;
                break;
            case 'i':
                algo->iterations = (int) strtol(optarg, NULL, 10);
                break;
            case 'I':
                algo->info = 1;
                break;
            case 'j':
                algo->landmarks = (int) strtol(optarg, NULL, 10);
                break;
            case 'J': /* very expert options help message */
                Usage(2);
                exit(EXIT_SUCCESS);
                break;
            case 'k':
                algo->constant = (double) strtod(optarg, NULL);
                break;
            case 'K':
                algo->mixture = (int) strtol(optarg, NULL, 10);
                break;
            case 'l':
                algo->leastsquares = 1;
                algo->varweight = 0;
                algo->hierarch = 0;
                break;
            case 'L':
                algo->instfile = 1;
                break;
//             case 'm':
//                 break;
            case 'M':
                bseA->mapfile_name = (char *) malloc((strlen(optarg) + 2) * sizeof(char));
                mystrncpy(bseA->mapfile_name, optarg, strlen(optarg) + 1);
                algo->alignment = 1;
                algo->atoms = 0;
                break;
            case 'n':
                algo->write_file = 0;
                break;
            case 'N':
                algo->nullrun = 1;
                break;
            case 'o':
                bseA->anchorf_name = (char *) malloc((strlen(optarg) + 2) * sizeof(char));
                mystrncpy(bseA->anchorf_name, optarg, strlen(optarg) + 1);
                break;
            case 'O':
                algo->olve = 1;
                break;
            case 'p':
                algo->precision = (double) strtod(optarg, NULL);
                break;
            case 'P':
                if (algo->atoms > 0)
                {
                    printf("\n\n    -> Only alpha carbons can be selected <-");
                    printf("\n    -> when calculating principle components <-\n");
                    algo->atoms = 0;
                }

                algo->pca = (double) strtod(optarg, NULL);
                break;
            case 'q':
                algo->morphfile = 1;
                break;
            case 'Q':
                algo->scalefactor = (double) strtod(optarg, NULL);
                break;
            case 'r':
                mystrncpy(algo->rootname, optarg, FILENAME_MAX - 1);
                break;
            case 'R':
                algo->random = (int) optarg[0];
                break;
            case 's':
                algo->selection = (char *) malloc((strlen(optarg) + 1) * sizeof(char));
                mystrncpy(algo->selection, optarg, FILENAME_MAX - 1);
                break;
            case 'S':
                algo->selection = (char *) malloc((strlen(optarg) + 1) * sizeof(char));
                mystrncpy(algo->selection, optarg, FILENAME_MAX - 1);
                algo->revsel = 1;
                break;
//             case 't':
//                 break;
            case 'T':
                algo->threads = (int) strtol(optarg, NULL, 10);
                break;
            case 'u':
                algo->mbias = 1;
                break;
            case 'U':
                algo->printlogL = 1;
                break;
            case 'v':
                algo->varweight = 1;
                algo->covweight = 0;
                algo->leastsquares = 0;
                break;
            case 'V':
                Version();
                exit(EXIT_SUCCESS);
                break;
            case 'w':
                if (isdigit(optarg[0]))
                    algo->weight = (int) strtol(optarg, NULL, 10);
                break;
            case 'W':
                algo->verbose = 1;
                break;
            case 'x':
                algo->noinnerloop = 1;
                break;
            case 'X':
                algo->seed = 1;
                break;
            case 'y':
                algo->doave = 0;
                break;
            case 'Y':
                algo->stats = 1;
                break;
            case 'z':
                algo->minc = (double) strtod(optarg, NULL);
                break;
            case 'Z':
                algo->princaxes = 0;
                break;
            case '?':
                printf("\n  Bad option '-%c' \n", optopt);
                Usage(0);
                exit(EXIT_FAILURE);
                break;
            default:
                abort();
        }
    }
}


static void
PrintSuperposStats(CdsArray *cdsA)
{
    int             i, j;
    int             newlen;
    const int       vlen = cdsA->vlen, cnum = cdsA->cnum;

    if (algo->stats)
    {
        if (algo->verbose)
        {
            for (i = 0; i < cnum; ++i)
            {
                printf("  -> radius of gyration, cds[%3d] = %8.3f \n", i+1, cdsA->cds[i]->radgyr);
                for (j = 0; j < 4; ++j)
                    printf("  -> eigenvalue[%d] = %10.3f \n", j, cdsA->cds[i]->evals[j]);
            }
        }

        printf("  -> radius of gyration of mean   %8.3f \n", cdsA->avecds->radgyr);
    }

    printf("    %d models superimposed in %.1f ms \n", cnum, algo->milliseconds);
    fflush(NULL);

    printf("  * Least-squares <sigma>                        %10.5f\n", stats->stddev);
    fflush(NULL);

    printf("  * Classical LS pairwise <RMSD>                 %10.5f\n", stats->ave_paRMSD);
    printf("  * Maximum Likelihood <sigma>                   %10.5f\n", stats->mlRMSD);
    printf("  ~ Marginal Log Likelihood                     %11.2f\n", stats->mlogL);
    printf("  ~ AIC                                         %11.2f\n", stats->AIC);
    printf("  ~ BIC                                         %11.2f\n", stats->BIC);
    printf("  + Rotational, translational, covar chi^2      %11.2f (P:%3.2e)\n",
           stats->chi2, chisqr_sdf(stats->chi2 * vlen * cnum * 3, vlen * cnum * 3, 0));

    if (algo->hierarch > 0 && algo->hierarch <= 5)
        printf("  + Hierarchical minimum var (sigma)               %3.2e (%3.2e)\n",
               2.0*stats->hierarch_p1 / (3*cnum + 2.0*(1.0 + stats->hierarch_p2)),
               sqrt(2.0*stats->hierarch_p1 / (3*cnum + 2.0*(1.0 + stats->hierarch_p2))));

    if (algo->hierarch)
    {
        if (algo->varweight)
        {
            if (vlen - 3 < 3*cnum - 6)
                newlen = vlen - 3;
            else
                newlen = 3*cnum - 6;
        }
        else
        {
            newlen = vlen - 3;
        }

        printf("  + Hierarchical var (%3.2e, %3.2e) chi^2 %11.2f (P:%3.2e)\n",
               stats->hierarch_p1, stats->hierarch_p2, stats->hierarch_chi2,
               chisqr_sdf(stats->hierarch_chi2 * newlen, newlen, 0));

        printf("  + Omnibus chi^2                               %11.2f (P:%3.2e)\n",
               (newlen * stats->hierarch_chi2 + vlen * cnum * 3 * stats->chi2) / (vlen * cnum * 3 + newlen),
               chisqr_sdf(newlen * stats->hierarch_chi2 + vlen * cnum * 3 * stats->chi2, (vlen * cnum * 3 + newlen), 0));
    }

    printf("  < skewness                                    %11.2f (P:%3.2e)\n",
           stats->skewness[3], 2.0 * normal_sdf(fabs(stats->skewness[3]/stats->SES), 0.0, 1.0));
    printf("  < skewness Z-value                            %11.2f\n", fabs(stats->skewness[3]/stats->SES));
    printf("  < kurtosis                                    %11.2f (P:%3.2e)\n",
           stats->kurtosis[3], 2.0 * normal_sdf(fabs(stats->kurtosis[3]/stats->SEK), 0.0, 1.0));
    printf("  < kurtosis Z-value                            %11.2f\n", fabs(stats->kurtosis[3]/stats->SEK));
    printf("  Data pts = %d,  Free params = %d,  D/P = %-5.1f\n",
           (int) stats->ndata, (int) stats->nparams, (stats->ndata / stats->nparams));

    printf("  * Median structure = #%d\n", stats->median + 1);
    printf("  N(total) = %d, N(atoms) = %d, N(structures) = %d\n",
           (cnum * vlen), vlen, cnum);
    printf("  Total rounds = %d\n", algo->rounds + 1);
    if (algo->rounds + 1 >= algo->iterations)
    {
        printf("\n   >> WARNING: Maximum iterations met. <<");
        printf("\n   >> WARNING: Failure to converge to requested precision. <<\n\n");
    }
    printf("  Converged to a fractional precision of %.1e\n", stats->precision);
    printf("I===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-==I\n");
    fflush(NULL);
}


void
leave(int sig)
{
    if (algo->rounds > 0)
    {
        printf("    Aborting at iteration %d ....\n", algo->rounds+1);
        fflush(NULL);

        algo->abort = 1;
        signal(sig, SIG_IGN);
    }
    else
    {
        fflush(NULL);
        signal(sig, SIG_DFL);
    }
}

