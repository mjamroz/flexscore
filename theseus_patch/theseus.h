/*
    Theseus - maximum likelihood superpositioning of macromolecular structures

    Copyright (C) 2004-2014 Douglas L. Theobald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the:

    Free Software Foundation, Inc.,
    59 Temple Place, Suite 330,
    Boston, MA  02111-1307  USA

    -/_|:|_|_\-
*/

#ifndef THESEUS_SEEN
#define THESEUS_SEEN


#if __STDC__ != 1
    #error NOT a Standard C environment
#endif

/* Temporary workaround for broken OSX system headers, shipped in 
   XCode 1.5. They're declaring stuff 'static inline', and this is 
   incompatible with the '-ansi' flag I pass to gcc.
   See Apple bug #3805571. */ 
#if defined(__APPLE__) && !defined(inline) 
    #define inline __inline__ 
#endif

#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <signal.h>
#include <dirent.h>
#include <sys/time.h>
#include <sys/stat.h>
#ifndef __MINGW32__
    #include <sys/resource.h>
#endif
#include <sys/types.h>
#include <gsl/gsl_sf_gamma.h>
#include "Error.h"
#include "CovMat.h"
#include "DLTmath.h"
#include "distfit.h"
#include "Embed.h"
#include "FragDist.h"
#include "pdbUtils.h"
#include "pdbMalloc.h"
#include "pdbStats.h"
#include "pdbIO.h"
#include "PCAstats.h"
#include "RandCds.h"
#include "MultiPose.h"
#include "MultiPose2MSA.h"
#include "MultiPoseMix.h"
#include "QuarticHornFrag.h"
#include "GibbsMet.h"
#include "ProcGSLSVDNu.h"
#include "termcol.h"

// Jamroz
#include "FlexscoreUtils.h"

static void
ParseCmdLine(int argc, char *argv[], CdsArray *baseA);

static void
PrintSuperposStats(CdsArray *cdsA);

#endif
